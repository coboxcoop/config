# config

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`config` generates a `.coboxrc` file which is stored in your home directory. It stores app configuration such as your nickname, storage directory and mount directory.

## Install

```
npm i -g @coboxcoop/config

```

## Usage

See `/tests`

## API
See swagger documentation... (we won't have this for a while).

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
