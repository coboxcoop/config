const debug = require('@coboxcoop/logger')('@coboxcoop/config')
const fs = require('fs')
const findUp = require('find-up')
const YAML = require('js-yaml')
const path = require('path')

/*
 * load a .coboxrc or .coboxseederrc yaml config file
 * @param {string} location - absolute or relative path
 */
function load (location) {
  let configPath
  if (location && !path.isAbsolute(location)) {
    // find-up does wierd things with absolute paths
    // so we check first before handling
    configPath = findUp.sync([location])
    if (!configPath) {
      console.log(`${location} does not exist, using empty config\n`)
      return {}
    }
  } else {
    configPath = location
    if (!fs.existsSync(configPath)) {
      console.log(`${configPath} does not exist, using empty config\n`)
      return {}
    }
  }
  let config = YAML.safeLoad(fs.readFileSync(configPath))
  debug({ msg: '.coboxrc config file loaded successfully' })
  return config || {}
}

/*
 * save a .coboxrc or .coboxseederrc yaml config file
 * @param {string} location - absolute or relative path
 * @param {object} param - the config object
 */
function save (location, config = {}) {
  if (!isObject(config)) throw new Error('config must be an object')
  if (fs.existsSync(location)) {
    debug({ msg: `writing to .coboxrc config file at ${location}` })
    return fs.writeFileSync(path.resolve(location), YAML.safeDump(config))
  }
  debug({ msg: 'creating a new .coboxrc config file' })
  return fs.writeFileSync(path.resolve(location), YAML.safeDump(config))
}

function isObject (obj) {
  return Object.prototype.toString.call(obj) === '[object Object]'
}

module.exports = { load, save }
